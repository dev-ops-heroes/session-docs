using Xunit;
using FluentAssertions;
using ClassLibrary;

namespace ClassLibraryTests
{
    public class Fizz_buzz_should
    {
        [Theory]
        [InlineData(2, "Fizz")]
        [InlineData(3, "Buzz")]
        [InlineData(5, "Jizz")]
        [InlineData(2 * 3, "FizzBuzz")]
        [InlineData(2 * 5, "FizzJizz")]
        [InlineData(3 * 5, "BuzzJizz")]
        [InlineData(2 * 3 * 5, "FizzBuzzJizz")]
        //You get the idea
        public void return_a_concatenation_of_values_for_each_divisor(int value, string expectedResult)
        {
            //Arrange
            var fizzBuzz = new FizzBuzz();

            //Act
            var result = fizzBuzz.Execute(value);

            //Assert
            result.Should().Be(expectedResult);
        }
    }
}

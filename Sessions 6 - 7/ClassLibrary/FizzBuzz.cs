﻿using System.Collections.Generic;

namespace ClassLibrary
{
    public class FizzBuzz
    {
        private static IReadOnlyDictionary<int, string> _map = new Dictionary<int, string>()
        {
            { 2, "Fizz" },
            { 3, "Buzz" },
            { 5, "Jizz" },
        };

        public string Execute(int value)
        {
            string result = "";
            foreach(var mapping in _map)
            {
                if (value % mapping.Key == 0)
                {
                    result += mapping.Value;
                }
            }
            return result;
        }
    }
}
